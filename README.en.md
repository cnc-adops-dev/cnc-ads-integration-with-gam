# CNC ads integration with GAM
## Implementation description
**For correct displaying of ads, the following implementation is necessary:**

In the HTML `<head>` section insert HTML code with configuration.
The configuration is different for the display type (mobile, desktop or responsive) and the content type of the page (homepage/category, article or gallery).

You will get the specific attributes for the configuration in the [`site-config.json`](site-config.json) configuration file (a sample is in the repository).

Next, you need to implement the ad-positions themselves (samples are in the ad-positions folder).

All ad-positions except [`leaderboard_1`](./ad-positions/leaderboard_1.html) have the same structure with different `class` and `id`.

The list of positions you will implement will be listed in the [`ad-position-placement.txt`](ad-positions-placement.txt) file you will receive (sample is in the repository).

The implementation of ad positions is different for the content type `template` (**category**, **article**, **gallery**) and the display type `webType` (**mobile**, **desktop**)

There are two sets of ad positions, for mobile and desktop view. This setup is for both non-responsive and responsive sites, with the caveat that if you have a **responsive site**, the value of the `webType` attribute is `webType="responsive"`.
* If you set the `webType` to **responsive** then you have the option to set the `responsiveBreakpoint` attribute, which determines what ad positions will be initialized to what dimension (if you don't set responsiveBreakpoint the default value is 576).

The [`leaderboard_1`](./ad-positions/leaderboard_1.html) position must be positioned above the content of the site and the element that wraps the content must have `class="branding-wrapper"`. The [`leaderboard_1`](./ad-positions/leaderboard_1.html) position is a desktop-only position.

A real example of the HTML configuration is in the [`page-definition-example.html`](page-definition-example.html) file.

---

## Values used in the configuration for advertising

#
### Values in the window.__cncPageDefinition object

`site = site ID : `_`string`_ (site ID - found in the configuration file)

`forceArea = 'homepage' | 'ostatni' | ...` _(content type: homepage = only homepage, ostatni = all other pages; options for your site can be found in the configuration file)_

`webType = 'mobile' | 'desktop' | 'responsive`'

`responsiveBreakpoint = null | integer` _(if webType = 'responsive', the responsiveBreakpoint parameter can be used to specify the break between the mobile view and the larger view, if not specified, the default value is set to 576px)_

`keywords = []` _(an array of keywords from the web `<meta keywords>`, e.g.: keywords = ["vanoce", "darky", "jezisek"])_

`template = 'category' | 'article' | 'gallery'` _(**category** = homepage or other article listings, **article** = article, **gallery** = gallery)_

# 
### Sample config file (you will receive a custom config)
_[site-config.json](site-config.json)_
```json
{
	"site": "isport",
	"forceArea": "homepage" | "ostatni" | "prvniliga"
}
```
#
### Sample list of ad positions that need to be inserted into the site for each template and display type _(you will receive your own list)_
```text
category
	mobile
		mobile_rectangle_2
		mobile_rectangle_3
		mobile_rectangle_4
		out_of_page
	desktop
		leaderboard_1
		commercial_1
		commercial_2
		halfpage_1
		halfpage_2
article
	mobile
		mobile_rectangle_2
		mobile_rectangle_3
		mobile_rectangle_4
		out_of_page
	desktop
		leaderboard_1
		commercial_1
		commercial_2
		halfpage_1
		halfpage_2
gallery
	mobile
		small_leaderboard
	desktop
		leaderboard_1
		halfpage_1
```

#
### Example of web implementation
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Title</title>

	<!--    CNC PAGE DEFINITION     -->

	<!-- Style for marking advertising positions and setting their default dimensions. If it is not loaded here, the script  "//w.cncenter.cz/cnc-wrapper.min.js" will load it on its own -->

	<link href="//w.cncenter.cz/styles/cnc-slot-sizes.min.css" rel="stylesheet" id="cnc_global_css">

	<script type="application/javascript">
		window.__cncPageDefinition = window.__cncPageDefinition || {};

/* 
In case of SPA (single page application) it is necessary to call the function for 
reloading the advertisement at each transition between pages 

Before that it is necessary to change the values in the object window.__cncPageDefinition always according to the target page (see example  SPA update) 
*/

		window.__cncPageDefinition.browserEngine = 'SSR';	// SSR | SPA
		window.__cncPageDefinition.site = '{site}';
		window.__cncPageDefinition.template = '{template}';	//category = homepage + other article listings, article, gallery

		window.__cncPageDefinition.webType = '{webType}';	//mobile, dekstop, responsive
		window.__cncPageDefinition.forceArea = '{forceArea}';	//homepage = only homepage, other = everything else
		window.__cncPageDefinition.responsiveBreakpoint = '{responsiveBreakpoint}';	//integer - breakpoint between mobile display and larger, if not filled in, the default value is 576px
		window.__cncPageDefinition.keywords = [];	//an array of keywords (as in <meta keywords>), e.g: ["christmas", "gifts","santa"] 
	</script>

	<!-- 
	The script below have dynamic URLs according to the site, where is placed on (you can get information about the correct value for your site from CNC)
		
	{site_cpex_prebid_settings}	
	-->
	

	<!-- 
		This script id=cnc_cpex_cmp ensures loading of the CMP bar 
	-->	
	<script id="cnc_cpex_cmp" src="//cdn.cpex.cz/cmp/v2/cpex-cmp.min.js" async="async" fetchpriority="high"></script>

	<script id="cnc_gpt" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" async="async"></script>

	<!-- 
		This script can be replaced by another in case of different implementation (more info will be provided by CNC if needed) 
	-->
	<script id="cnc_cpex_prebid_settings" src="//micro.rubiconproject.com/prebid/dynamic/22918.js" async="async" referrerpolicy="strict-origin-when-cross-origin" ></script>

	<script id="cnc_wrapper" type="application/javascript" src="//w.cncenter.cz/cnc-wrapper.min.js" async="async"></script>


	<!--	CNC PAGE DEFINITION	-->
	<!--	example SPA update	-->
	<script type="application/javascript">
		// Call when the page is first loaded, as well as when switching between pages

/*
This is just an example of how a function for updating __cncPageDefinition and subsequently calling __cncAdsNewAdSlots, which handles ad reloading, might look.
(The function __cncAdsNewAdSlots is called directly if __cncAdsInitialized = true is met, or via the ad queue __cncAdsQueue.)
The website owner can use this pre-prepared function or create their own version. 
However, it is crucial that the __cncAdsNewAdSlots function is called only after the __cncPageDefinition has been populated with updated data and that this call is made through the ad queue.
*/
		
		const changePageAds = (props) => {
			if (!props || typeof props !== 'object') return;

			const pageDefinition = window.__cncPageDefinition || {};
			const configurableFields = ['template', 'webType', 'forceArea', 'responsiveBreakpoint', 'keywords'];

			configurableFields.forEach(key => {
				if (props[key] !== undefined) {
						pageDefinition[key] = props[key];
				}
			});

			window.__cncPageDefinition = pageDefinition;

			const initializeAdSlots = () => {
				if (typeof window.__cncAdsNewAdSlots === 'function') {
						window.__cncAdsNewAdSlots();
				}
			};

			window.__cncAdsInitialized 
				? initializeAdSlots()
				: (window.__cncAdsQueue = window.__cncAdsQueue || []).push(initializeAdSlots);
		};
	
	/* 
	// usage

	const props = {
		template: 'article',	
		forceArea: 'zpravy',	
		keywords: ['keyword1', 'keyword2']
	};

	changePageAds(props);
	*/
	</script>
	<!-- example SPA update	-->
	
	<body>
		<!-- advertising position (leaderboard_1) -->
		<div id="cnc_branding_creative_wrapper">
			<div class="cnc-ads cnc-ads--leaderboard">
				<div class="cnc-ads__within" id="cnc_leaderboard_1"></div>
		</div>
	</div>

	<!-- it is necessary that the element that wraps the entire web content has class="branding-wrapper"   -->

	 <div class="branding-wrapper">
		<!-- web content + advertising positions -->

		<!-- advertising positions (halfpage_1) -->
		<div class="cnc-ads cnc-ads--halfpage">
			<div class="cnc-ads__within" id="cnc_halfpage_1"></div>
		</div>

		<!-- advertising positions (halfpage_repeater) -->
		<div class="cnc_side_ad_position"></div>

		<!-- advertising positions (rectangle_480_1) -->
		<div class="cnc-ads cnc-ads--rectangle_480_1">
			<div class="cnc-ads__within" id="cnc_rectangle_480_1"></div>
		</div>

		<!-- etc. - all positions are listed in the folder ad-positions -->

	</div>
</body>
</html>
```
#
### Example of implementation into a website with responsive display / hiding of positions

```html
<!DOCTYPE html>
<html lang="cs">
	<head>
<!-- 
	Custom implementation of a style for hiding elements according to viewport’s width
	Specify custom width (10/0px in the example) of breakpoint for mobile/desktop view
	Class naming for the DIV wrapper .ad-position-mobile/.ad-position-mobile  is arbitrary, it is not bound to anything else, you can name it anything 
-->

		<style>
			.ad-position--desktop, .ad-position--tablet, .ad-position--mobile {
				display: none;
			}

			@media only screen and (min-width: 1080px) {
				.ad-position-desktop {
					display: block;
				}
			}
	
			@media (min-width: 768px) and (max-width: 1079px) {
				.ad-position--tablet {
					display: block;
				}
			}

			@media only screen and (max-width: 767px) {
				.ad-position--mobile {
					display: block;
				}
			}
		</style>
	</head>
	<body>
		<div class="ad-position-desktop">

	<!-- desktop advertising position (leaderboard_1) -->
			<div id="cnc_branding_creative_wrapper">
				<div class="cnc-ads cnc-ads--leaderboard">
					<div class="cnc-ads__within" id="cnc_leaderboard_1"></div>
				</div>
			</div>
		</div>

			<!-- it is necessary that the element that wraps the entire web content has class="branding-wrapper"   -->
		<div class="branding-wrapper">
			<!-- DIV wrapper for desktop display -->
			<div class="ad-position-desktop">

			<!-- advertising positions for desktop display -->
				<div class="cnc-ads cnc-ads--rectangle_480_1">
					<div class="cnc-ads__within" id="cnc_rectangle_480_1"></div>
				</div>

			</div>

			<!-- DIV wrapper for mobile display -->
			<div class="ad-position--mobile ad-position--tablet">

				<!-- advertising positions for mobile display -->
				<div class="cnc-ads cnc-ads--mobile_rectangle_2">
					<div class="cnc-ads__within" id="cnc_mobile_rectangle_2"></div>
				</div>

			</div>
		</div>
</body>
</html>
```
